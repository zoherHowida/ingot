<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\CategoryType;

class CategoryTypeController extends Controller
{
    public function index() {

        try {

            return response()->json([
                'categoryType' => CategoryType::orderBy('created_at', 'Desc')->get()
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Data please Try again'
            ], 417);

        }


    }
}
