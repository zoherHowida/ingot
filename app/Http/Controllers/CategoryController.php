<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Validator;

class CategoryController extends Controller
{
    public function index() {

        try {
            return response()->json([
                'category' => Category::myCategory()->with('type')->orderBy('created_at', 'Desc')->get()
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Data please Try again'
            ], 417);

        }


    }

    public function create(Request $request) {

        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100|unique:categories',
                'type_id' => 'required|exists:category_types,id',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $category = Category::create([
                'name'=> $request->name,
                'type_id'=> $request->type_id,
                'user_id'=> Auth::user()->id,
            ]);

            return response()->json([
                'message' => 'Category successfully Created',
                'category' => $category
            ], 200);


        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Add Category please Try again'
            ], 417);

        }

    }

}
