<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;

class CurrencyController extends Controller
{
    public function index() {

        try {

            return response()->json([
                'currency' => Currency::get()
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Currency please Try again'
            ], 417);

        }


    }
}
