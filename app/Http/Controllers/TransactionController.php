<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Category;
use App\Models\CategoryType;
use App\Traits\AnalysisTrait;
use App\Traits\BalanceTrait;
use Carbon\Carbon;
use Validator;
use Auth;

class TransactionController extends Controller
{
    use AnalysisTrait, BalanceTrait;

    public function index() {

        try {

            $transactionsData = Transaction::with('category')->transactionBy(Auth::user()->id)->orderBy('created_at', 'Desc')->get();
            $transactiona = [];
            foreach($transactionsData as $key => $value) {
                $transactiona[] = [
                    'id' => $key+1,
                    'amount'=> Auth::user()->currency->shortName. ' ' .$value->amount,
                    'note'=> $value->note,
                    'category'=> $value->category->name,
                    'created'=> $value->created_at->format('d-m-Y')
                ];
            }

            return response()->json([
                'transaction' => $transactiona
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Transaction please Try again'
            ], 417);

        }

    }

    public function analysisChart() {
        try {

            $byDate = Carbon::now()->subDays(30)->isoFormat('YYYY-MM-DD');
            $byMonth = Carbon::now()->subMonth(12)->isoFormat('YYYY-MM-DD');

            $chartDateIncomes =  Transaction::incomes(Auth::user()->id)->whereDate('created_at', '>=', $byDate)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%d") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            $chartMonthIncomes =  Transaction::incomes(Auth::user()->id)->whereDate('created_at', '>=', $byMonth)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%M") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            $chartYearIncomes =  Transaction::incomes(Auth::user()->id)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%Y") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            $chartDateExpenses =  Transaction::expenses(Auth::user()->id)->whereDate('created_at', '>=', $byDate)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%d") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            $chartMonthExpenses =  Transaction::expenses(Auth::user()->id)->whereDate('created_at', '>=', $byMonth)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%M") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            $chartYearExpenses =  Transaction::expenses(Auth::user()->id)
                ->select(\DB::raw('COUNT(*) as count, DATE_FORMAT(created_at, "%Y") as date'))
                ->groupBy('date')
                ->orderBy('date', 'Asc')
                ->get();

            return response()->json([
                'analysisChart' => [
                    'incomes' => [
                        'byYear' => $chartYearIncomes,
                        'byMonth' => $chartMonthIncomes,
                        'byDate' => $chartDateIncomes
                    ],
                    'expenses' => [
                        'byYear' => $chartYearExpenses,
                        'byMonth' => $chartMonthExpenses,
                        'byDate' => $chartDateExpenses
                    ]

                ]
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Data please Try again'
            ], 417);

        }

    }

    public function analysisByUser(Request $request)
    {

        try {
            return response()->json([
                'analysis' => $this->analysis(Auth::user()->id)
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error in Retrive Data please Try again'
            ], 417);

        }


    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'amount' => 'required|string|min:0.1',
                'categoryId' => 'required|string|exists:categories,id',
                'note' => 'required|string|max:50',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $category = Category::find($request->categoryId);
            if (!$category) return response()->json(['category' => ["NotFound."]], 400);

            $checkBalance = $this->CheckBalance($category->type_id, Auth::user()->id, $request->amount);

            if($checkBalance['status'] !== 200){

                return response()->json([
                    'message' => $checkBalance['message']
                ], 417);
            }

            if($checkBalance['code'] == 0) {
                return response()->json(['amount' => [$checkBalance['message']]], 400);
            }

            $category = Transaction::create([
                'amount' => $request->amount,
                'category_id' => $request->categoryId,
                'user_id' => Auth::user()->id,
                'note' => $request->note,
            ]);

            return response()->json([
                'message' => 'Transaction successfully Created',
                'analysis' => $this->analysis(Auth::user()->id)
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'message' => 'Error Add Transaction please Try again'
            ], 417);

        }


    }
}
