<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\carbon;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    public function index() {
        try {
            $users = Cache::get('users');
            if($users == null) {
                $users = [];
                $userData = User::user()->orderBy('created_at', 'Desc')->get();
                foreach($userData as $user) {
                    $users[] = [
                        'transaction' => $user->transaction,
                        'name'=> $user->name,
                        'email'=> $user->email,
                        'phone'=> $user->phone,
                        'birthdate'=> $user->birthDate,
                        'totalExpenses'=> $user->totalExpenses,
                        'totalIncomes'=> $user->totalIncomes,
                        'registeredDate'=> $user->created_at->format('d-m-Y')
                    ];
                }
                Cache::set('users',$users);
                $expiresAt = now()->addMinutes(60);
                Cache::put('users', null, $expiresAt);
            }
            return response()->json([
                'users' => $users
            ], 200);

        }
        catch(\Exception $e) {
            return response()->json([
                'message' => 'Error in Retrive Data please Try again'
            ], 417);
        }

    }
}
