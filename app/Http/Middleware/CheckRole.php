<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Enums\Role;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param $role
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        $roleNumber = 0;
        if ($role == 'Admin') $roleNumber = Role::Admin;
        if ($role == 'User') $roleNumber = Role::User;
        if (Auth::user()->role == $roleNumber) {
            return $next($request);
        }

        return response()->json([
            'message' => 'You don\'t Have Any Permission'
        ], 417);
    }
}
