<?php
namespace App\Traits;

use App\Models\Transaction;
use Auth;

trait AnalysisTrait
{

    public function analysis($userId) {

        $allTransaction = Transaction::transactionBy($userId)->count();
        $incomes = Transaction::incomes($userId)->sum('amount');
        $expenses = Transaction::expenses($userId)->sum('amount');

        return [
            'count' => $allTransaction,
            'incomes' => $incomes,
            'expenses' => $expenses,
            'balance' => $incomes - $expenses
        ];

    }

}
