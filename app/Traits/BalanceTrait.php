<?php
namespace App\Traits;

use App\Models\Transaction;
use Auth;

trait BalanceTrait
{

    public function CheckBalance($categoryType, $userId, $amount) {

        /**
         * If Code Equal 1 it is User Can Add transaction
         */
        $arr = collect([
            'status' => 200,
            'code' => 0,
            'message' => ''
        ]);

        try {
            if ($categoryType == 2) {


                $incomes = Transaction::incomes($userId)->sum('amount');
                $expenses = Transaction::expenses($userId)->sum('amount');
                if ($incomes - $expenses < $amount) {
                    $arr['message'] = 'There is not enough balance';
                    return $arr;
                }
            }
            $arr['code'] = 1;
            return $arr;

        } catch (\Exception $e) {
            $arr['status'] = 400;
            $arr['message'] = 'An error occurred while processing the transaction';
            return $arr;
        }
    }

}
