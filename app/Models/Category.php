<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\CategoryType;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type_id',
        'user_id',
    ];

    protected $appends = [
        'typeName',
    ];

    public function getTypeNameAttribute(){

        $type= CategoryType::find($this->type_id);
        if ($type) {
            return $type->name;
        }
        return 'N/Y';
    }


    public function type() {
        return $this->belongsTo('App\Models\CategoryType','type_id','id');
    }

    public function scopeMyCategory(Builder $builder) {
        return $builder->where('user_id', '=', Auth::user()->id);
    }


}
