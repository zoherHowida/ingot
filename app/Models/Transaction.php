<?php

namespace App\Models;

use App\Scopes\SortCreatedAt;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Enums\CategoryType as CategoryTypeEnum;
use Auth;

class Transaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'category_id',
        'user_id',
        'note',
    ];

    public function category() {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User','useR_id','id');
    }

    public function scopeTransactionBy(Builder $builder, $userId) {
        return $builder->where('user_id', '=', $userId);
    }

    public static function incomes($userId){
        if($userId == 0) {
            return Transaction::whereHas('category', function ($query) {
                return $query->where('type_id', '=', CategoryTypeEnum::Incomes);
            });

        }
        return Transaction::where('user_id', $userId)->whereHas('category', function ($query) {
            return $query->where('type_id', '=', CategoryTypeEnum::Incomes);
        });

    }

    public static function expenses($userId){

        if($userId == 0) {
            return Transaction::whereHas('category', function ($query) {
                return $query->where('type_id', '=', CategoryTypeEnum::Expenses);
            });
        }
        return Transaction::where('user_id', $userId)->whereHas('category', function ($query) {
            return $query->where('type_id', '=', CategoryTypeEnum::Expenses);
        });

    }

}
