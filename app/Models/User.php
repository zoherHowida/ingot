<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Transaction;
use App\Enums\Role;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $appends = [
        'fullUrlImage',
        'totalExpenses',
        'totalIncomes'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthDate',
        'phone',
        'userImage',
        'walletCurrency',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function currency() {
        return $this->belongsTo('App\Models\Currency','walletCurrency','id');
    }

    public function getFullUrlImageAttribute(){
        if($this->userImage == null) return null;
        return asset('storage/'.$this->userImage);
    }

    public function transaction() {
        return $this->hasMany('App\Models\Transaction','user_id');
    }


    public function getTotalExpensesAttribute(){
        return Transaction::expenses($this->id)->sum('amount');
    }

    public function getTotalIncomesAttribute(){
        return Transaction::incomes($this->id)->sum('amount');
    }

    public function scopeUser(Builder $builder) {
        return $builder->where('role', '=', Role::User);
    }


}
