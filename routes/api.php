<?php

//header('Access-Control-Allow-Origin: http://localhost:53843');
//header('Access-Control-Allow-Origin: http://localhost');
//header('Access-Control-Allow-Credentials: true');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header('Access-Control-Allow-Headers: *,x-xsrf-token,token,content-type');


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryTypeController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});

Route::post('getCurrency', [CurrencyController::class, 'index']);

Route::group(['middleware' => ['auth:api']], function(){

    Route::group(['middleware' => ['role:Admin']], function() {
        Route::post('/admin/user', [UserController::class, 'index']);
    });

    Route::group(['middleware' => ['role:User']], function() {
        Route::post('/getCategoryType', [CategoryTypeController::class, 'index']);

        Route::group(['prefix' => 'category'], function() {
            Route::post('/', [CategoryController::class, 'index']);
            Route::post('/create', [CategoryController::class, 'create']);
        });

        Route::group(['prefix' => 'transaction'], function() {
            Route::post('/', [TransactionController::class, 'index']);
            Route::post('/analysis', [TransactionController::class, 'analysisByUser']);
            Route::post('/analysisChart', [TransactionController::class, 'analysisChart']);
            Route::post('/create', [TransactionController::class, 'create']);
        });
    });



});
