<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Currency;
use App\Models\CategoryType;
use App\Models\User;
use App\Enums\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@Ingot.com',
            'password' => bcrypt('Admin123'),
            'phone' => '0790000000',
            'role' => Role::Admin
        ]);

        CategoryType::create([
            'name' => 'incomes'
        ]);
        CategoryType::create([
            'name' => 'expenses'
        ]);

        Currency::create([
            'name' => 'Jordan Dinar',
            'shortName' => 'JD'
        ]);
        Currency::create([
            'name' => 'Dollar',
            'shortName' => '$'
        ]);
    }
}
