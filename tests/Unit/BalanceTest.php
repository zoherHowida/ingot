<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use App\Traits\BalanceTrait;
use Tests\TestCase;


class BalanceTest extends TestCase
{
    use BalanceTrait;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {

        // Check Balance For User Id 1

        /**
         * Please Add In DataBase
         * Check Balance For user id 1
         * and category Id 1
         */

        /**
         * The Trait function CheckBalance Take 3 Paramter
         * categoryType, userId, amount
         */

        $checkBalance = $this->CheckBalance(2, 3, 100);
        echo $checkBalance;
        $this->assertTrue(true);
    }
}
