import { Injectable } from '@angular/core';
import {CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import {LocalStorageService} from '../localStorage.service';
/**
 * @date 2020-06-16
 * @author Zouheir Owaida <zoher.owida1@gmail.com>
 */

@Injectable({
  providedIn: 'root'
})
export class UnAuthenticatedGuard implements CanActivate, CanLoad {

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkIsAuth();
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {

    return this.checkIsAuth(route.path);
  }

  checkIsAuth(route = null) {

    try {
      const userInfo = this.localStorageService.getLocalDataObject('userInformation');
      if (userInfo != null ){
        this.router.navigate(['/dashboard/default']);
        return false;
      }
      return true
    } catch (e) {
      return false;
    }

  }

}
