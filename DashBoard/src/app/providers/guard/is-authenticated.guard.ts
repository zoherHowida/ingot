import { Injectable } from '@angular/core';
import {CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import {LocalStorageService} from '../localStorage.service';
import {AuthService} from '../auth.service';
import {Role} from '../../models/role';
/**
 * @date 2020-06-16
 * @author Zouheir Owaida <zoher.owida1@gmail.com>
 */

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedGuard implements CanActivate, CanLoad {

  constructor(
      private router: Router,
      private localStorageService: LocalStorageService,
      private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.authService.isAuthorized()) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    const roles = route.data.roles as Role[];
    if (roles && !roles.some(r => this.authService.hasRole(r))) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    return true;
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.authService.isAuthorized()) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    const roles = route.data && route.data.roles as Role[];
    if (roles && !roles.some(r => this.authService.hasRole(r))) {
      this.router.navigate(['/auth/login']);

      return false;
    }

    return true;
  }

}
