import { Injectable } from '@angular/core';
import { ServerName } from '../../serverConfiguration/env';
import { AccessControlAllowOrigin } from '../../serverConfiguration/env';
import { LocalStorageService } from '../localStorage.service';
import {TranslateService} from '@ngx-translate/core';
/**
 * @date 2020-06-16
 * @author Zouheir Owaida <zoher.owida1@gmail.com>
 */

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  private Token;
  private UserIsAuth = false;
  BaseURL = ServerName;
  MainHeader = {
    Accept: 'application/json'

  };
  Header: { Accept: string; "Access-Control-Allow-Origin": string } = {
    // @ts-ignore
    'Content-type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': AccessControlAllowOrigin
  };

  getToken() {
    return this.Token;
  }

  getUserIsAuth() {
    return this.UserIsAuth;
  }

  constructor(
    private localStorageService: LocalStorageService,
    private translate: TranslateService
  ) {}


  APIURL(Name: string, withAuth = true, withLang, contentType) {


    const data = this.localStorageService.getLocalDataObject('userInformation');

    if(contentType == null) {
      this.Header = {
        Accept: this.Header['Accept'],
        'Access-Control-Allow-Origin':this.Header['Access-Control-Allow-Origin']
      };

    }
    if (data !== null) {
      this.Token = data.access_token;
    }


    if (withAuth) {
      this.Header['Authorization'] = 'Bearer ' + this.Token;
    }

    if (withLang) {

      if (this.translate.currentLang === 'en') {
        this.Header['Accept-Language'] = 'en-us';

      } else {
        this.Header['Accept-Language'] = 'ar-sa';

      }

    }

    const output = {
      URL: null,
      header: this.Header,
    };

    switch (Name) {
      case 'login':
        output.URL = '/auth/login';
        break;
      case 'logout':
        output.URL = '/auth/logout';
        break;
      case 'register':
        output.URL = '/auth/register';
        break;
      case 'getCurrency':
        output.URL = '/getCurrency';
        break;
      case 'getCategories':
        output.URL = '/category';
        break;
      case 'getCategoryType':
        output.URL = '/getCategoryType';
        break;
      case 'addCategory':
        output.URL = '/category/create';
        break;
      case 'getAnalysisTransaction':
        output.URL = '/transaction/analysis';
        break;
      case 'getAnalysisTransactionChart':
        output.URL = '/transaction/analysisChart';
        break;
      case 'addTransaction':
        output.URL = '/transaction/create';
        break;
      case 'getTransaction':
        output.URL = '/transaction';
        break;
      case 'getUser':
        output.URL = '/admin/user';
        break;
      default:
        output.URL = 'N/Y';
    }
    output.URL  = this.BaseURL + output.URL;
    return output;
  }
}
