import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ApiService } from './api.service';
import { LocalStorageService } from '../localStorage.service';
import { TranslateService } from '@ngx-translate/core';
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";

/**
 * @date 2020-07-07
 * @author Zouheir Owaida <zoher.owida1@gmail.com>
 */

@Injectable({
  providedIn: 'root'
})


export class DataServiceService {

  coordination;
  httpOptions;

  constructor(
    private httpClient: HttpClient,
    private apiService: ApiService,
    private  localStorageService: LocalStorageService,
    private translate: TranslateService,
    private localStorage: LocalStorageService,
    private router: Router,
    public toster: ToastrService,

  ) { }

  unauthenticated() {
    this.localStorage.saveLocalObject('userInformation', null);
    this.router.navigateByUrl('/auth/login');

  }

  async config(URLName, extension, WithAuth, WithLang, contentType, header) {

    this.coordination = await this.apiService.APIURL(URLName, WithAuth, WithLang, contentType);

    if (extension !== null) {
      this.coordination.URL = this.coordination.URL + extension;
    }


    if (WithAuth && this.coordination.header.Authorization === 'Bearer ') {

      this.toster.error('Please Try Again.');
      return false;
    }
    if (header !== null) {
      this.coordination.header[header.key] = header.value;
    }
    if (contentType !== '' && contentType !== null) {
      this.coordination.header['Content-type'] = contentType;
    }
    this.httpOptions = {
      headers: new HttpHeaders(this.coordination.header),
      withCredentials: false,
    };

    console.log(URLName + ' : Start : ' + new Date());

    return true;

  }

  async sendGetRequest(URLName,
                       extension = null,
                       WithAuth = true,
                       WithLang = false,
                       header = null,
                       contentType = ''): Promise<Observable<any>> {

    if (! await this.config(URLName, extension, WithAuth, WithLang, contentType, header) ) {
      return of(null);
    } else {
      console.log({
        Method: 'GET',
        AuthIsRequired: WithAuth,
        urlName: URLName,
        coordination: this.coordination,
        extension,
        header: this.coordination.header
      });

      return this.httpClient.get(this.coordination.URL, this.httpOptions)
        .pipe(
          map(resData => {

            return {status: 200, data: resData };
          }),
          catchError(error => {

            console.log(URLName + ' : End : ' + new Date());
            console.log('Error' + ' : Is : ' + error.statusText);
            return of({status: error.status, data: error});

          }));
    }
  }

  async sendPostRequest(URLName,
                        data: any,
                        extension = null,
                        WithAuth = true,
                        WithLang = false,
                        header = null,
                        contentType = ''): Promise<Observable<any>> {

    if (! await this.config(URLName, extension, WithAuth, WithLang, contentType, header) ) {
      return of(null);
    } else {
      console.log({
        Method: 'POST',
        AuthIsRequired: WithAuth,
        urlName: URLName,
        coordination: this.coordination,
        data,
        extension,
        header: this.coordination.header
      });

      return this.httpClient.post(this.coordination.URL, data, this.httpOptions)
        .pipe(
          map(resData => {
            return {status: 200, data: resData };
          }),
          catchError(error => {

            if ([401].includes(error.status)) {
              if (error.error.message === 'Unauthenticated.') {
                this.unauthenticated();
                this.router.navigateByUrl('/auth/login');
                return of(null);
              }
            } else if([417].includes(error.status)) {
              this.toster.error(error.error.message);
              return of(null);
            }

            console.log(URLName + ' : End : ' + new Date());
            console.log('Error' + ' : Is : ' + error.statusText);

            // tslint:disable-next-line:triple-equals

            return of({status: error.status, data: error});

          }));

    }

  }
}

