import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor() { }

    saveLocalData(key, value)
    {
        try {
            localStorage.setItem(key, value);
        } catch (e) {
            // this.alert.presentToast('', 'Sorry, your Device does not support Web Storage...');
            return null;
        }
    }

    getLocalData(key)
    {
        try {
            return localStorage.getItem(key);
        } catch (e) {
            // this.alert.presentToast('', 'Sorry, your Device does not support Web Storage...');
            return null;
        }
    }

    getLocalDataObject(key)
    {
        try {
            const itemValue = localStorage.getItem(key);
            if (itemValue !== 'undefined' && itemValue != null && itemValue !== 'null')
            {
                return JSON.parse(localStorage.getItem(key));
            } else {
                return null;
            }

        } catch (e) {

            // this.alert.presentToast('', 'Sorry, your Device does not support Web Storage...');
            return null;
        }

    }

    saveLocalObject(key, value)
    {
        try {
            localStorage.setItem(key, JSON.stringify(value));
        } catch (e) {
            // this.alert.presentToast('', 'Sorry, your Device does not support Web Storage...');
        }
    }

    deleteLocalData(key)
    {
        try {
            localStorage.setItem(key, null);
        } catch (e) {
            // this.alert.presentToast('', 'Sorry, your Device does not support Web Storage...');
        }
    }

}
