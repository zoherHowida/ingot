import { Injectable, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import {DataServiceService} from './requestApi/data-service.service';
import {LocalStorageService} from './localStorage.service';
import {Role} from '../models/role';

export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  public userData: any;
  public user: firebase.User;
  // tslint:disable-next-line:variable-name
  private _sessionId: string;
  public showLoader = false;

  constructor(
    public router: Router,
    public toster: ToastrService,
    private cookieService: CookieService,
    private dataService: DataServiceService,
    private localStorage: LocalStorageService
  ) {

    const user = localStorage.getLocalDataObject('userInformation');
    if (user) {
      this.userData = user;
      this._sessionId = this.userData;
      cookieService.set('userInformation', JSON.stringify(this.userData));
      localStorage.saveLocalObject('userInformation', this.userData);
      // localStorage.setItem('userInformation', JSON.stringify(this.userData));
      // JSON.parse(localStorage.getItem('userInformation'));
      this.router.navigateByUrl('/dashboard/default');
    } else {
      this.localStorage.deleteLocalData('userInformation');
      // localStorage.setItem('userInformation', null);
      // JSON.parse(localStorage.getItem('userInformation'));
    }

  }

  // tslint:disable-next-line:contextual-lifecycle
  ngOnInit(): void { }


  async SignOut() {

    (await this.dataService.sendPostRequest('logout', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.localStorage.deleteLocalData('userInformation');
          this.router.navigateByUrl('/auth/login');
          location.reload();

        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            this.toster.error('Error');
          }
        }
      }
    });


  }

  get isLoggedIn(): boolean {
    const user = this.localStorage.getLocalDataObject('userInformation');
    return (user != null) ? true : false;
  }

  get currency() {
    const user = this.localStorage.getLocalDataObject('userInformation');
    return (user != null) ? user.user.currency.shortName : 'N/Y';
  }

  isAuthorized() {
    const user = this.localStorage.getLocalDataObject('userInformation');
    return user !== null;
  }

  hasRole(role: Role) {
    const user = this.localStorage.getLocalDataObject('userInformation');

    if (user !== null) {
      return user.user.role === role;
    }
    return false;

  }

}
