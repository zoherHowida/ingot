import { Routes } from '@angular/router';
import {Role} from '../../models/role';
import {IsAuthenticatedGuard} from '../../providers/guard/is-authenticated.guard';

export const content: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../../components/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [IsAuthenticatedGuard],
    canLoad: [IsAuthenticatedGuard],
    data: {
      breadcrumb: 'Dashboard'
    }
  },
  {
    path: 'category',
    loadChildren: () => import('../../INGOTPages/category/category.module').then(m => m.CategoryModule),
    canActivate: [IsAuthenticatedGuard],
    canLoad: [IsAuthenticatedGuard],
    data: {
      breadcrumb: 'Category',
      roles: [
        Role.User,
      ]
    }
  },
  {
    path: 'transaction',
    loadChildren: () => import('../../INGOTPages/transactions/transactions.module').then(m => m.TransactionsModule),
    canActivate: [IsAuthenticatedGuard],
    canLoad: [IsAuthenticatedGuard],
    data: {
      breadcrumb: 'Transaction',
      roles: [
        Role.User,
      ]

    }
  },
];
