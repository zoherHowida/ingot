import { Injectable, HostListener } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';

// Menu
export interface Menu {
	path?: string;
	title?: string;
	icon?: string;
	type?: string;
	badgeType?: string;
	badgeValue?: string;
	active?: boolean;
  bookmark?: boolean;
  role?: string;
	children?: Menu[];
}

@Injectable({
	providedIn: 'root'
})

export class NavService {

	public screenWidth: any
	public collapseSidebar: boolean = false
	public fullScreen = false;

	constructor() {
		this.onResize();
		if (this.screenWidth < 991) {
			this.collapseSidebar = true;
		}
	}

	// Windows width
	@HostListener('window:resize', ['$event'])
	onResize(event?) {
		this.screenWidth = window.innerWidth;
	}

	MENUITEMS: Menu[] = [
    {
      path: '/dashboard/default', title: 'Dashboard', icon: 'home', type: 'link'
    },
    {
      path: '/category', title: 'Category', icon: 'list', type: 'link', role: 'User'
    },
    {
      path: '/transaction', title: 'Transaction', icon: 'dollar-sign', type: 'link', role: 'User'
    },
	]
	// Array
	items = new BehaviorSubject<Menu[]>(this.MENUITEMS);


}
