import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ContentLayoutComponent } from './shared/components/layout/content-layout/content-layout.component';
import { FullLayoutComponent } from './shared/components/layout/full-layout/full-layout.component';
import { content } from "./shared/routes/content-routes";
import { full } from './shared/routes/full.routes';
import { AdminGuard } from './shared/guard/admin.guard';
import {UnAuthenticatedGuard} from "./providers/guard/un-authenticated.guard";
import {IsAuthenticatedGuard} from "./providers/guard/is-authenticated.guard";
import {RegisterComponent} from "./auth/register/register.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard/default',
    pathMatch: 'full'
  },
  {
    path: 'auth/login',
    component: LoginComponent,
    canActivate: [UnAuthenticatedGuard],
    canLoad: [UnAuthenticatedGuard],

  },
  {
    path: 'auth/register',
    component: RegisterComponent,
    canActivate: [UnAuthenticatedGuard],
    canLoad: [UnAuthenticatedGuard],
  },
  {
    path: '',
    component: ContentLayoutComponent,
    canActivate: [IsAuthenticatedGuard],
    canLoad: [IsAuthenticatedGuard],
    children: content
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [IsAuthenticatedGuard],
    canLoad: [IsAuthenticatedGuard],
    children: full
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'legacy'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
