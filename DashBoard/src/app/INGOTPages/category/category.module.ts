import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import { CreateComponent } from './create/create.component';

@NgModule({
  declarations: [CategoryComponent, CreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    CategoryRoutingModule,
    NgxDatatableModule,
    ReactiveFormsModule,


  ]
})
export class CategoryModule { }
