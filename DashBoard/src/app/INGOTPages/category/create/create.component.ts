import { Component, OnInit } from '@angular/core';
import {DataServiceService} from "../../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

type UserFields = 'name' | 'type_id';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  public categoryForm: FormGroup;
  public formErrors: FormErrors = {
    'name': '',
    'type_id': '',
  };
  public errorMessage: any;
  public types = [];

  constructor(
    private dataService: DataServiceService,
    public toster: ToastrService,
    private fb: FormBuilder,
    private router: Router,

  ) {
    this.getType();
    this.categoryForm = fb.group({
      name: [null, [Validators.required]],
      type_id: [null, Validators.required]
    });

  }

  ngOnInit(): void {

  }

  async getType() {
    (await this.dataService.sendPostRequest('getCategoryType', [], null, true, false)).subscribe(async resData => {

      console.log(resData);
      if (resData !== null) {
        if (resData.status === 200) {
          this.types = resData.data.categoryType;
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          }else{
            this.toster.error('Error');
          }
        }
      }
    });
  }

  async add() {
    (await this.dataService.sendPostRequest('addCategory', this.categoryForm.value, null, true, false)).subscribe(async resData => {

      console.log(resData);
      if (resData !== null) {
        if (resData.status === 200) {
          this.toster.success(resData.data.message);
          await this.router.navigateByUrl('/category');
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          }  else if ([400].includes(resData.status)){
            this.toster.error(resData.data.error.name);
          } else {
            this.toster.error('Error');
          }
        }
      }
    });

  }

}
