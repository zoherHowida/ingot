import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryComponent } from './category.component';
import { CreateComponent } from './create/create.component'
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CategoryComponent,
        data: {
          title: "Category",
          breadcrumb: ""
        }
      },
      {
        path: 'create',
        component: CreateComponent,
        data: {
          title: "Category",
          breadcrumb: "Create"
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
