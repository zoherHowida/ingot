import { Component, OnInit } from '@angular/core';
import {DataServiceService} from "../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  public category = [];
  constructor(
    private dataService: DataServiceService,
    public toster: ToastrService,
  ) {
    this.getCategories();
  }

  async getCategories() {

    (await this.dataService.sendPostRequest('getCategories', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          console.log(resData.data.category);
          this.category = resData.data.category;
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            this.toster.error('Error');
          }
        }
      }
    });

  }

  ngOnInit(): void {

  }

}
