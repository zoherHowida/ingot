import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { TransactionsRoutingModule } from './transactions-routing.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { TransactionsComponent } from './transactions.component';
import {SharedModule} from '../../shared/shared.module';
import {ChartistModule} from 'ng-chartist';
import {CountToModule} from 'angular-count-to';
import {CreateComponent} from "./create/create.component";

@NgModule({
  declarations: [TransactionsComponent, CreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    TransactionsRoutingModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    SharedModule,
    ChartistModule,
    CountToModule,
  ]
})
export class TransactionsModule { }
