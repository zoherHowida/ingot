import { Component, OnInit } from '@angular/core';
import {DataServiceService} from "../../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../../providers/auth.service";

type UserFields = 'amount' | 'category' | 'note';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  public transactionForm: FormGroup;
  public formErrors: FormErrors = {
    'amount': '',
    'category': '',
    'note': '',
  };
  public errorMessage: any;
  public categories = [];

  constructor(
    private dataService: DataServiceService,
    public toster: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    public authService: AuthService


  ) {
    this.transactionForm = fb.group({
      amount: [null, [Validators.required,Validators.pattern('[0-9]{1,}(\.[0-9]{1,})?$')]],
      categoryId: [null, [Validators.required]],
      note: [null, Validators.required]
    });

  }

  async ngOnInit() {
    await this.getCategories();
  }

  async getCategories() {

    (await this.dataService.sendPostRequest('getCategories', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          console.log(resData.data.category);
          this.categories = resData.data.category;
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            this.toster.error('Error');
          }
        }
      }
    });

  }

  async add() {
    (await this.dataService.sendPostRequest('addTransaction', this.transactionForm.value, null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.toster.success(resData.data.message);
          this.router.navigateByUrl('/transaction');
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            for (let key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });
  }


}
