import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransactionsComponent } from './transactions.component';
import {CreateComponent} from "./create/create.component";
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: TransactionsComponent,
        data: {
          title: "Transaction",
          breadcrumb: ""
        }
      }
    ]
  },
  {
    path: 'create',
    component: CreateComponent,
    data: {
      title: "Transaction",
      breadcrumb: "Create"
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
