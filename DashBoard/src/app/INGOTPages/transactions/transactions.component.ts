import { Component, OnInit } from '@angular/core';
import * as chartData from '../../shared/data/dashboard/default';
import {DataServiceService} from "../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";
import { AuthService } from '../../providers/auth.service'

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public chart1 = chartData.chartBox1;
  public chart2 = chartData.chartBox2;
  public chart3 = chartData.chartBox3;
  public data = [];
  public transaction = [];

  constructor(
    private dataService: DataServiceService,
    public toster: ToastrService,
    public authService: AuthService
  ) {
  }

  async ngOnInit() {
    await this.getAnalysis();
    await this.getTransaction();
  }

  async getAnalysis() {

    (await this.dataService.sendPostRequest('getAnalysisTransaction', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.data = [
            {
              label: 'Count of Transaction',
              currency: null,
              amount: parseFloat(resData.data.analysis.count),
              icon: null,
              chart: this.chart1
            },
            {
              label: 'Incomes',
              currency: this.authService.currency,
              amount: resData.data.analysis.incomes,
              icon: null,
              chart: this.chart2
            },
            {
              label: 'Expenses',
              currency: this.authService.currency,
              amount: resData.data.analysis.expenses,
              icon: null,
              chart: this.chart3
            },
            {
              label: 'Your Balance',
              currency: this.authService.currency,
              amount: resData.data.analysis.balance,
              icon: null,
              chart: this.chart1
            }
          ];
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            console.log(resData.data.error);
            for (let key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });

  }

  async getTransaction() {

    (await this.dataService.sendPostRequest('getTransaction', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.transaction = resData.data.transaction;
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            console.log(resData.data.error);
            for (let key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });

  }
}
