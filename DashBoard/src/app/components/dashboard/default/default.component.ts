import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as chartData from './../../../shared/data/dashboard/default';
import {DataServiceService} from '../../../providers/requestApi/data-service.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../providers/auth.service';
import {Role} from '../../../models/role';

declare var require: any;
const Knob = require('knob'); // browserify require

// @ts-ignore
const primary = localStorage.getItem('primary_color') || '#4466f2';
// @ts-ignore
const secondary = localStorage.getItem('secondary_color') || '#1ea6ec';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DefaultComponent implements OnInit {

  public data = [];
  public dataChart = [];
  public users = [];
  public role = Role;
  public filters = [
    {
      id: 1,
      label: 'Day',
      parameterName: 'byDate'
    },
    {
      id: 2,
      label: 'Month',
      parameterName: 'byMonth'
    },
    {
      id: 3,
      label: 'Year',
      parameterName: 'byYear'
    },
  ];
  public chartBox = [
    {
      label: 'Incomes',
      color: 'bg-primary'
    },
    {
      label: 'Expenses',
      color: 'bg-secondary'
    }
  ];
  filterName = 'Day';
  // Chart Data
  public chart1 = chartData.chartBox1;
  public chart2 = chartData.chartBox2;
  public chart3 = chartData.chartBox3;



  public chart4 = {
    type: 'Line',
    data: {},
    options: {
      low: 0,
      showArea: true,
      fullWidth: true,
      onlyInteger: true,
      chartPadding: {
        left: 0,
        right: 0,
      },
      axisY: {
        low: 0,
        scaleMinSpace: 50,
      },
      axisX: {
        showGrid: false
      }
    },
    events: {
      created: (data) => {
        const defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
          id: 'gradient1',
          x1: 0,
          y1: 0,
          x2: 1,
          y2: 1
        }).elem('stop', {
          offset: 0,
          'stop-color': primary
        }).parent().elem('stop', {
          offset: 1,
          'stop-color': secondary
        });
      }
    }
  };


  constructor(
    private dataService: DataServiceService,
    public toster: ToastrService,
    public authService: AuthService

  ) {}

  async ngOnInit() {

    if (this.authService.hasRole(Role.User)) {
      await this.getAnalysis();
      await this.getAnalysisChart();
    } else {
      await this.getUsers();
    }
  }

  async getAnalysis() {

    (await this.dataService.sendPostRequest('getAnalysisTransaction', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.data = [
            {
              label: 'Count of Transaction',
              currency: null,
              amount: resData.data.analysis.count,
              icon: null,
              chart: this.chart1
            },
            {
              label: 'Incomes',
              currency: this.authService.currency,
              amount: resData.data.analysis.incomes,
              icon: null,
              chart: this.chart2
            },
            {
              label: 'Expenses',
              currency: this.authService.currency,
              amount: resData.data.analysis.expenses,
              icon: null,
              chart: this.chart3
            },
            {
              label: 'Your Balance',
              currency: this.authService.currency,
              amount: resData.data.analysis.balance,
              icon: null,
              chart: this.chart1
            }
          ];
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            console.log(resData.data.error);
            for (const key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });

  }

  async getAnalysisChart() {

    (await this.dataService.sendPostRequest('getAnalysisTransactionChart', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.dataChart = resData.data;
          this.filterChart(1);
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            console.log(resData.data.error);
            for (const key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });
  }

  async getUsers(){

    (await this.dataService.sendPostRequest('getUser', [], null, true, false)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          const that = this;
          this.users = resData.data.users;
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          } else {
            console.log(resData.data.error);
            for (const key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }

          }
        }
      }
    });

  }

  filterChart(id) {

    const filter = this.filters.find(x => x.id == id);

    this.filterName = filter.label;
    const incomes = this.dataChart.analysisChart.incomes[filter.parameterName];
    const expenses = this.dataChart.analysisChart.expenses[filter.parameterName];

    let labels = [];
    const incomesValue = [];
    const expensesValue = [];

    incomes.forEach(function(value) {
      labels.push(value.date);
    });
    expenses.forEach(function(value) {
      labels.push(value.date);
    });
    // Remove Duplicate
    labels = labels.filter((element, i) => i === labels.indexOf(element));

    labels.forEach(function(value) {
      const incomesVal = incomes.find(x => x.date == value);
      const expensesVal = expenses.find(x => x.date == value);
      if (incomesVal) {incomesValue.push(incomesVal.count); } else {incomesValue.push(0); }
      if (expensesVal) {expensesValue.push(expensesVal.count); } else {expensesValue.push(0); }

    });

    this.chart4.data = {
      labels,
      series: [
        incomesValue,
        expensesValue
      ]
    };
  }

}
