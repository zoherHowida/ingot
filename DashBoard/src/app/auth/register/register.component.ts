import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import {DataServiceService} from "../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";
import {LocalStorageService} from "../../providers/localStorage.service";
import {DatePipe} from "@angular/common";

type UserFields = 'name' | 'email' | 'number'| 'birthDate'| 'userImage'| 'walletCurrency'| 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public  walletCurrency = [];
  public registerForm: FormGroup;
  public formErrors: FormErrors = {
    'name': '',
    'email': '',
    'number': '',
    'birthDate': '',
    'userImage': '',
    'walletCurrency': '',
    'password': '',
  };
  public errorMessage: any;
  public errorNumber: boolean = true;
  public nowDate = this.dateFormat.transform(new Date(), 'yyyy-MM-dd');

  constructor(
              private afauth: AngularFireAuth,
              private fb: FormBuilder,
              private router: Router,
              private dataService: DataServiceService,
              public toster: ToastrService,
              private localStorage: LocalStorageService,
              private dateFormat: DatePipe
  ) {

    this.registerForm = fb.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      birthDate: [null, Validators.required],
      userImage: [null, Validators.required],
      walletCurrency: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  async ngOnInit() {
    await this.getWalletCurrency();
  }

  fileEvent(e){
    this.registerForm.patchValue({userImage: e.target.files[0]});
  }

  async  getWalletCurrency() {


    (await this.dataService.sendPostRequest('getCurrency', [], null, false, false)).subscribe(async resData => {

      if(resData != null) {
        if (resData.status === 200) {
          this.walletCurrency = resData.data.currency;
        }
      }
    });

  }

  async register() {

    if (!this.errorNumber) {
      this.toster.error('Phone number format error');
      return;
    }
    var myFormData = new FormData();
    myFormData.append('name', this.registerForm.get('name').value);
    myFormData.append('email', this.registerForm.get('email').value);
    myFormData.append('phone', this.registerForm.get('phone').value);
    myFormData.append('birthDate', this.registerForm.get('birthDate').value);
    myFormData.append('userImage', this.registerForm.get('userImage').value);
    myFormData.append('walletCurrency', this.registerForm.get('walletCurrency').value);
    myFormData.append('password', this.registerForm.get('password').value);

    (await this.dataService.sendPostRequest('register', myFormData, null, false, false, null, null)).subscribe(async resData => {
      if (resData !== null) {
        if (resData.status === 200) {
          this.toster.success('Success Register Please login Now ');
          await this.router.navigateByUrl('/auth/login');
        }
        else {
          if ([401, 422].includes(resData.status)) {
            this.toster.error(resData.data.error.error);
          }  else if ([400].includes(resData.status)){
            for (let key in resData.data.error) {
              this.toster.error(resData.data.error[key][0]);
            }
          }  else {
            this.toster.error('Error');
          }
        }
      }
    });
  }

  hasErrorNumber(error) {
    this.errorNumber = error;
  }

}
