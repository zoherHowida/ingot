import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import {DataServiceService} from "../../providers/requestApi/data-service.service";
import {ToastrService} from "ngx-toastr";
import {LocalStorageService} from "../../providers/localStorage.service";

type UserFields = 'email' | 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user: firebase.User;
  public loginForm: FormGroup;
  public formErrors: FormErrors = {
    'email': '',
    'password': '',
  };
  public errorMessage: any;

  constructor(
    private afauth: AngularFireAuth, private fb: FormBuilder,
    private router: Router,
    private dataService: DataServiceService,
    public toster: ToastrService,
    private localStorage: LocalStorageService

  ) {
    this.loginForm = fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  // Simple Login
  async login() {

    (await this.dataService.sendPostRequest('login', this.loginForm.value, null, false, false)).subscribe(async resData => {

      console.log(resData);
      if (resData !== null) {
        if (resData.status === 200) {
          await this.loggedIn(resData.data);
        }
        else {
          if ([401, 422].includes(resData.status)) {
              this.toster.error(resData.data.error.error);
          } else {
            this.toster.error('Error');
          }
        }
      }
    });
  }

  async loggedIn(data) {

    this.localStorage.saveLocalObject('userInformation', data);
    await this.router.navigateByUrl('/dashboard/default');
    location.reload();

  }

}
